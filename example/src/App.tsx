// @ts-nocheck
import React, { useEffect, useRef, useState } from 'react';

import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
} from 'react-native';
import {
  DepthCamera,
  checkDeviceSupport,
  DepthCameraRefType,
  ITakePhotoResponse,
} from 'react-native-logmeal-depth-sdk';
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import { Dirs, FileSystem } from 'react-native-file-access';

enum DepthSupport {
  supported = 'supported',
  unsupported = 'unsupported',
  checking = 'checking',
}

type FileState =
  | 'checking'
  | 'exists'
  | 'canread'
  | 'doesntexist'
  | 'cannottread';

const App = () => {
  const [takePhotoResult, setTakePhotoResult] =
    useState<ITakePhotoResponse | null>(null);
  const [rgbFileExists, setRgbFileExists] = useState<FileState>('checking');
  const [depthFileExists, setDepthFileExists] = useState<FileState>('checking');
  const [rawDepthFileExists, setRawDepthFileExists] =
    useState<FileState>('checking');
  const [confidenceMapExists, setConfidenceMapExists] =
    useState<FileState>('checking');
  const [isDepthSupported, setIsDepthSupported] = useState<DepthSupport>(
    DepthSupport.checking
  );
  const [captureError, setCaptureError] = useState(null);
  const ref = useRef<DepthCameraRefType>(null);

  const _checkDeviceSupport = async () => {
    const cameraPermissionCheck = await check(PERMISSIONS.ANDROID.CAMERA);
    if (cameraPermissionCheck !== RESULTS.GRANTED) {
      await request(PERMISSIONS.ANDROID.CAMERA);
    }
    const checkDeviceSupportResult = await checkDeviceSupport();
    setIsDepthSupported(
      checkDeviceSupportResult
        ? DepthSupport.supported
        : DepthSupport.unsupported
    );
  };

  const _takePhoto = () => {
    setTakePhotoResult(null);
    setCaptureError(null);
    if (ref.current) {
      setDepthFileExists('checking');
      setRgbFileExists('checking');

      ref.current
        .takePhoto()
        .then((tpResult: ITakePhotoResponse) => {
          setTakePhotoResult(tpResult);

          // RGB
          FileSystem.exists(tpResult.rgbImageUri, 'base64').then((exists) => {
            setRgbFileExists(exists ? 'exists' : 'doesntexist');
            if (exists) {
              FileSystem.readFile(tpResult.rgbImageUri, 'base64').then((text) =>
                setRgbFileExists(text ? 'canread' : 'cannottread')
              );
            }
          });

          // Depth
          FileSystem.exists(tpResult.depthData.uri, 'base64').then((exists) => {
            setDepthFileExists(exists ? 'exists' : 'doesntexist');
            if (exists) {
              FileSystem.readFile(tpResult.depthData.uri, 'base64').then(
                (text) => setDepthFileExists(text ? 'canread' : 'cannottread')
              );
            }
          });

          // RawDepth
          if (tpResult.extra.rawDepthData?.uri) {
            FileSystem.exists(tpResult.extra.rawDepthData.uri, 'base64').then(
              (exists) => {
                setRawDepthFileExists(exists ? 'exists' : 'doesntexist');
                if (exists) {
                  FileSystem.readFile(
                    tpResult.extra.rawDepthData.uri,
                    'base64'
                  ).then((text) =>
                    setRawDepthFileExists(text ? 'canread' : 'cannottread')
                  );
                }
              }
            );
          }

          // ConfidenceMap
          if (tpResult.extra.confidenceMapUri) {
            FileSystem.exists(tpResult.extra.confidenceMapUri, 'base64').then(
              (exists) => {
                setConfidenceMapExists(exists ? 'exists' : 'doesntexist');
                if (exists) {
                  FileSystem.readFile(
                    tpResult.extra.confidenceMapUri,
                    'base64'
                  ).then((text) =>
                    setConfidenceMapExists(text ? 'canread' : 'cannottread')
                  );
                }
              }
            );
          }
        })
        .catch((error) => {
          setRgbFileExists('doesntexist');
          setDepthFileExists('doesntexist');
          setCaptureError(error?.message ?? 'ERROR!');
        });
    }
  };

  useEffect(() => {
    _checkDeviceSupport();
  }, []);

  const _fileStateToEmoji = (fileState: FileState) => {
    switch (fileState) {
      case 'checking':
        return '⏳';
      case 'exists':
        return '✅❌';
      case 'canread':
        return '✅✅';
      case 'doesntexist':
        return '❌❌';
      case 'cannottread':
        return '❌';
    }
  };

  return (
    <ScrollView style={styles.container}>
      {isDepthSupported === DepthSupport.checking ? (
        <View style={styles.box}>
          <Text>Checking support</Text>
        </View>
      ) : isDepthSupported === DepthSupport.supported ? (
        <DepthCamera style={styles.box} ref={ref} />
      ) : (
        <View style={styles.box}>
          <Text>NOT Supported</Text>
        </View>
      )}

      <TouchableOpacity style={styles.touchable} onPress={_takePhoto}>
        <Text>Capture Depth Data</Text>
      </TouchableOpacity>

      <View style={styles.stateChangersContainer}>
        <TouchableOpacity
          style={styles.stateChangerBtn}
          onPress={() => setIsDepthSupported(DepthSupport.supported)}
        >
          <Text>{DepthSupport.supported}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.stateChangerBtn}
          onPress={() => setIsDepthSupported(DepthSupport.unsupported)}
        >
          <Text>{DepthSupport.unsupported}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.stateChangerBtn}
          onPress={() => setIsDepthSupported(DepthSupport.checking)}
        >
          <Text>{DepthSupport.checking}</Text>
        </TouchableOpacity>
      </View>
      {captureError && (
        <View style={styles.resultsContainer}>
          <Text style={styles.resultsTitle}>ERROR</Text>
          <Text>{captureError}</Text>
        </View>
      )}
      {takePhotoResult && (
        <View style={styles.resultsContainer}>
          <View style={[styles.section]}>
            <Text style={styles.resultsTitle}>Dirs</Text>
            <Text>{JSON.stringify(Dirs)}</Text>
          </View>

          <View style={[styles.section, styles.bgSlate]}>
            <Text style={styles.resultsTitle}>Focal length</Text>
            <Text>{JSON.stringify(takePhotoResult.depthData.focalLength)}</Text>
          </View>

          <View style={[styles.section]}>
            <Text style={styles.resultsTitle}>Principal point</Text>
            <Text>
              {JSON.stringify(takePhotoResult.depthData.principalPoint)}
            </Text>
          </View>

          <View style={[styles.section, styles.bgSlate]}>
            <Text style={styles.resultsTitle}>Camera pose</Text>
            <Text>{JSON.stringify(takePhotoResult.cameraPose)}</Text>
          </View>

          <View style={[styles.section]}>
            <Text style={styles.resultsTitle}>Depth Image Size</Text>
            <Text>{JSON.stringify(takePhotoResult.depthData.size)}</Text>
          </View>

          <View style={[styles.section, styles.bgSlate]}>
            <Text style={styles.resultsTitle}>
              Depth Image Uri - {_fileStateToEmoji(depthFileExists)}
            </Text>
            <Text>{takePhotoResult.depthData.uri}</Text>
          </View>

          <View style={[styles.section]}>
            <Text style={styles.resultsTitle}>
              RawDepth - {_fileStateToEmoji(rawDepthFileExists)}
            </Text>
            <Text>
              FL:{' '}
              {JSON.stringify(takePhotoResult.extra.rawDepthData?.focalLength)}
            </Text>
            <Text>
              PP:{' '}
              {JSON.stringify(
                takePhotoResult.extra.rawDepthData?.principalPoint
              )}
            </Text>
            <Text>
              {JSON.stringify(takePhotoResult.extra.rawDepthData?.size)}
            </Text>
            <Text>{takePhotoResult.extra.rawDepthData?.uri}</Text>
          </View>

          <View style={[styles.section, styles.bgSlate]}>
            <Text style={styles.resultsTitle}>
              ConfidenseMap - {_fileStateToEmoji(confidenceMapExists)}
            </Text>
            <Text>{takePhotoResult.extra.confidenceMapUri}</Text>
          </View>

          <View style={[styles.section]}>
            <Text style={styles.resultsTitle}>
              Image RGB Uri - {_fileStateToEmoji(rgbFileExists)}
            </Text>
            <Text>{takePhotoResult.rgbImageUri}</Text>
            <Image
              source={{ uri: takePhotoResult.rgbImageUri }}
              style={styles.image}
            />
          </View>
        </View>
      )}
    </ScrollView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  stateChangersContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  box: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e3e3e3',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width,
  },
  touchable: {
    marginTop: 15,
    width: 150,
    height: 50,
    backgroundColor: '#60A5FA',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 20,
  },
  stateChangerBtn: {
    marginTop: 15,
    height: 25,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D6D3D1',
    paddingHorizontal: 3,
  },
  resultsContainer: {
    marginTop: 15,
    flex: 1,
  },
  resultsTitle: {
    fontWeight: 'bold',
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e3e3e3',
    width: 250,
    height: 250,
  },
  section: {
    paddingHorizontal: 5,
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bgSlate: {
    backgroundColor: '#D1D5DB',
  },
});
