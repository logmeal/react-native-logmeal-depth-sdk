//
//  LMDepthSDK.m
//  LogmealDepthSdk
//
//  Created by Eduard Calero on 9/7/22.
//  Copyright © 2022 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(LMDepthSDK, RCTEventEmitter)
    RCT_EXTERN_METHOD(checkDeviceSupport: (RCTPromiseResolveBlock) resolve rejecter: (RCTPromiseRejectBlock) reject)
@end
