# LogMealDepth

[![CI Status](https://img.shields.io/travis/edudoonamis/LogMealDepth.svg?style=flat)](https://travis-ci.org/edudoonamis/LogMealDepth)
[![Version](https://img.shields.io/cocoapods/v/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)
[![License](https://img.shields.io/cocoapods/l/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)
[![Platform](https://img.shields.io/cocoapods/p/LogMealDepth.svg?style=flat)](https://cocoapods.org/pods/LogMealDepth)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
- iOS 14.0+
- Xcode 13+
- Swift 5.0+

## Installation

LogMealDepth is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LogMealDepth'
```

Then install your pods form the comand line, running the following comand form the podfile directory:

`
    pod install --repo-update
`

## Usage

### Conforming to LMDepthLibrary protocol

To use LMDepthLibrary you must conform the LMDepthLibraryDelegate protocol.

```swift
import UIKit
import AVFoundation
import LogMealDepth

extension TestViewController: LMDepthLibraryDelegate {
    func didGetMetadata(_ metadataDic: [String : Any]) {
        print(metadataDic)
    }
    
    func didCaptureDepthImage(_ image: UIImage?) {
        imageView.image = image
        if let image = image {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
    
    
    func didSaveDocuments(inUrls urls: [URL]) {
        
        let items = urls.compactMap { $0 as? AnyObject }
        
        ShareHelper.share(items: items,
                          in: self,
                          presentationStyle: .popover)
    }
    
    
}
```


The didMetadata callback returns the metadata dictionary with all the camera metadata.
The didCaptureDepthImage returns the depthImage captured as grayscale UIImage representation.
The didSaveDocuments returns the urls where the png document and the pdf document with all optical data is stored. It is recomended to be stored or downloaded somewhere to be used later.


### LMDepthLibrary configuration
In order to LMDepthLibrary be initialized you must configure them before its usage. Its is recomended to subclass a view as PreviewView and use it to show the UIImage Depth representation as the following example:

```swift
import UIKit
import AVFoundation
import LogMealDepth

class TestViewController: UIViewController {

    @IBOutlet weak var previewView: PreviewView!
    @IBOutlet weak var imageView: UIImageView!
    
    var deppLib: LMDepthLibrary!
    
    var imageTimer: Timer?
    
    @IBAction func onCaptureButtonTapped(_ sender: Any) {
        do {
            try deppLib.captureImage()
        } catch {
            print(error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deppLib = LMDepthLibrary()
        deppLib.configure(delegate: self, preview: previewView)
        
        if deppLib.isDeviceReadyToCaptureDeepthData() {
           print("DEVICE READY")
        } else {
            print("DEVICE IS NOT READY")
        }
       
    }
}

```



## Author

LogMeal © 2022. All Rights Reserved. | contact@logmeal.es
