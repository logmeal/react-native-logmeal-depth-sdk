//
//  LMDepthLibraryError.swift
//  LogMealDepth
//
//  Created by Eduard Calero on 3/7/22.
//

import Foundation

public enum LMDepthLibraryError: Error {
    case setupResultUnsuccessful
    case videoDeviceNotFound
    case videoDeviceInputError(Error)
    case cantAddInputError
    case cantAddOutputError
    case depthDataDeliveryisNotSuported
    case depthDataDeliveryisNotEnabled
    case incorrectImageScale
    case notPhotoDataFound
    case notDepthDataMapFound
    case notIntrisicMatrixdataFound
    case notCameraCalibrationDataFound
    case unableToWriteDepthPNGfile
    case unableToWriteRGBfile
    case unableToGetPixelBufferBaseAddress
}
