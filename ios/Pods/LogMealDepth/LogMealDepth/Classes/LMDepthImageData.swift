//
//  LMDepthImageData.swift
//  LogMealDepth
//
//  Created by Eduard Calero on 3/7/22.
//

import Foundation
import simd

public typealias DepthImageWidth = CGFloat
public typealias DepthImageHeight = CGFloat

public typealias DepthImage = CVPixelBuffer
public typealias PrincipalPoint = CGPoint
public typealias FocalDistance = CGPoint

public struct LMDepthImageData {
    public let dimensions: CGSize
    public let values: DepthImage
    public let principalPoint: PrincipalPoint
    public let focalDistance: FocalDistance
    public let accuracy: String
    public let correctedCenter: CGPoint
    public let cameraPose: matrix_float4x4

    public var dic: [String: Any] {
        var depthDataDic = [String: Any]()

        depthDataDic["depthFocalLengthX"] = self.focalDistance.x
        depthDataDic["depthFocalLengthY"] = self.focalDistance.y

        depthDataDic["depthPrincipalPointX"] = self.correctedCenter.x
        depthDataDic["depthPrincipalPointY"] = self.correctedCenter.y

        // is awlays inversed
        depthDataDic["depthImageWidth"] = self.dimensions.height;
        depthDataDic["depthImageHeight"] = self.dimensions.width;

        depthDataDic["cameraPose"] = self.matrixToArray(matrix: self.cameraPose)

        depthDataDic["values"] = self.pixelValues
        depthDataDic["accuracy"] = self.accuracy
        depthDataDic["notCorrectedDepthPrincipalPointX"] = self.principalPoint.x
        depthDataDic["notCorrectedDepthPrincipalPointY"] = self.principalPoint.y

        return depthDataDic
    }

    public var pixelValues: String {

        var pxValues = ""

                    let width = Int(dimensions.width)
                    let height = Int(dimensions.height)

        CVPixelBufferLockBaseAddress(values, .readOnly)
                      let floatBuffer = unsafeBitCast(CVPixelBufferGetBaseAddress(values), to: UnsafeMutablePointer<Float16>.self)

                        for y in 0...height-1 {
                            for x in 0...width-1 {
                          let pixel = floatBuffer[y * width + x]
                            pxValues.append(contentsOf: "\(pixel), ")
                        }
                      }


        CVPixelBufferUnlockBaseAddress(values, .readOnly)

        return pxValues
    }

    public var NSdic: NSDictionary {
        return self.dic as NSDictionary
    }

    private func matrixToArray(matrix: matrix_float4x4) -> [Float] {
        var result: [Float] = []

        for row in 0..<4 {
            for col in 0..<4 {
                result.append(matrix[row][col])
            }
        }

        return result
    }
}

