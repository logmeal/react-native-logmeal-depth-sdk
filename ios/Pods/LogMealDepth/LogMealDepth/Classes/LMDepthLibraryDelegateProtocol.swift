//
//  LMDepthLibraryDelegateProtocol.swift
//  LogMealDepth
//
//  Created by Eduard Calero on 3/7/22.
//

import UIKit


public protocol LMDepthLibraryDelegateProtocol {
    func didCaptureRGBImage(_ image: Result<UIImage?, Error>)
    func didCaptureRGBImageInURL(_ image: Result<URL, Error>)
    func didGetMetadata(_ metadataDic: Result<[String: Any], Error>)
    func didGetDephData(_ depthMetadata: Result<LMDepthImageData, Error>)
//    func didSaveDocuments(inUrls urls: [URL])
    func didCaptureDepthImage(_ image: Result<UIImage?, Error>)
    func didCaptureDepthImageInURL(_ image: Result<URL, Error>)
}
