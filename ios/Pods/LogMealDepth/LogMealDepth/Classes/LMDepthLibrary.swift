//
//  LMDepthLibrary.swift
//  DeepDetector
//
//  Created by Eduard Calero on 8/4/22.
//

import AVFoundation
import Photos
import UIKit
import simd

public class LMDepthLibrary: NSObject {

    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
        case criticalThermalState
    }

    private var photoData: Data?

    var previewView = PreviewView()
    var delegate: LMDepthLibraryDelegateProtocol!
    var preset: AVCaptureSession.Preset = .init(rawValue: "AVCaptureSessionPreset640x480")

    var urlsToShare = [URL]()

    private var setupResult: SessionSetupResult = .success


     var session = AVCaptureSession()
    private let sessionQueue = DispatchQueue(label: "session queue",
                                     qos: .userInitiated,
                                     attributes: [],
                                     autoreleaseFrequency: .workItem)



    private let dataOutputQueue = DispatchQueue(label: "video data queue",
                                                qos: .userInitiated,
                                                attributes: [],
                                                autoreleaseFrequency: .workItem)


    private var videoDeviceInput: AVCaptureDeviceInput!

    private let depthDataOutput = AVCaptureDepthDataOutput()
    private let builtInCameraOutput = AVCapturePhotoOutput()
    private var photoSettings = AVCapturePhotoSettings()
    private var configurationError: Error?

    var deepthData: AVDepthData?
    var deepMapImage: UIImage?

    private let videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInTrueDepthCamera,
                                                                                                .builtInDualWideCamera,
                                                                                                .builtInDualCamera,
                                                                                                .builtInUltraWideCamera,
                                                                                                .builtInWideAngleCamera],
                                                                               mediaType: .video,
                                                                               position: .back)

    //This function must be called after the sessionConfigurationProcess
    public func isDeviceAbleToCaptureDeepthData() -> Bool {

        guard !videoDeviceDiscoverySession.devices.isEmpty else {
            return false
        }

        return true
    }

    public func configure(delegate: LMDepthLibraryDelegateProtocol,
                          preview: PreviewView,
                          preset: AVCaptureSession.Preset = .init(rawValue: "AVCaptureSessionPreset640x480")) {
        self.delegate = delegate
        self.previewView = preview
        self.preset = preset

        DispatchQueue.main.async {
            self.previewView.videoPreviewLayer.session = self.session
        }

        // Check video authorization status, video access is required
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            break

        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant video access
             We suspend the session queue to delay session setup until the access request has completed
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
            })

        default:
            // The user has previously denied access
            setupResult = .notAuthorized
        }


//        let photos = PHPhotoLibrary.authorizationStatus()
//            if photos == .notDetermined {
//              sessionQueue.suspend()
//                PHPhotoLibrary.requestAuthorization({status in
//                    if status != .authorized{
//                        self.setupResult = .notAuthorized
//                    }
//                    self.sessionQueue.resume()
//                })
//            }



        let initialThermalState = ProcessInfo.processInfo.thermalState
        if initialThermalState == .serious || initialThermalState == .critical {
//            showThermalState(state: initialThermalState)

            setupResult = .criticalThermalState

            //Waits 30 secons to let the system to inform the user if its needed
            DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
                fatalError("Serious or Critical thermal State")
            }

        }

        /*
         Setup the capture session.
         In general it is not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.

         Why not do all of this on the main queue?
         Because AVCaptureSession.startRunning() is a blocking call which can
         take a long time. We dispatch session setup to the sessionQueue so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async {
            try? self.configureSession()
        }


    }


    private func startSession(){
        if setupResult == .success {
            self.session.startRunning()
        }

    }

     private func stopSession(){
        sessionQueue.async {
//            if self.setupResult == .success {
                self.session.stopRunning()
//            }
        }
    }




    private func configureSession() throws {

        configurationError = nil

        guard setupResult == .success else {
            configurationError = LMDepthLibraryError.setupResultUnsuccessful
            throw LMDepthLibraryError.setupResultUnsuccessful
        }


        let defaultVideoDevice: AVCaptureDevice? = videoDeviceDiscoverySession.devices.first

        guard let videoDevice = defaultVideoDevice else {
            print("Could not find any video device")
            setupResult = .configurationFailed
            configurationError = LMDepthLibraryError.videoDeviceNotFound
            throw LMDepthLibraryError.videoDeviceNotFound
        }

        do {
            //Creates the device input from the videDevice
            videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
        } catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            configurationError = LMDepthLibraryError.videoDeviceInputError(error)
            throw LMDepthLibraryError.videoDeviceInputError(error)
        }

        session.beginConfiguration()

        //https://stackoverflow.com/questions/19422322/method-to-find-devices-camera-resolution-ios
        session.sessionPreset = preset

        // Add a video input
        guard session.canAddInput(videoDeviceInput) else {
            print("Could not add video device input to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            configurationError = LMDepthLibraryError.cantAddInputError
            throw LMDepthLibraryError.cantAddInputError
        }

        session.addInput(videoDeviceInput)


        // Add a depth data output
        if session.canAddOutput(builtInCameraOutput) {
            session.addOutput(builtInCameraOutput)
        } else {
            print("Could not add built in camera output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            configurationError = LMDepthLibraryError.cantAddOutputError
            throw LMDepthLibraryError.cantAddOutputError
        }


//        builtInCameraOutput.connection(with: .video)?.isCameraIntrinsicMatrixDeliveryEnabled = true

        guard builtInCameraOutput.isDepthDataDeliverySupported else {
            print("Could not add built in camera output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            configurationError = LMDepthLibraryError.depthDataDeliveryisNotSuported
            throw LMDepthLibraryError.depthDataDeliveryisNotSuported
        }

        builtInCameraOutput.isDepthDataDeliveryEnabled = builtInCameraOutput.isDepthDataDeliverySupported


        session.commitConfiguration()
        setupResult = .success
        configurationError = nil

        self.session.startRunning()
        print("Session STARTED")

    }


    public func captureImage() throws {
        guard setupResult == .success else {
            if let configurationError = configurationError {
                throw configurationError
            } else {
                throw LMDepthLibraryError.setupResultUnsuccessful
            }
        }

        photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        photoSettings.flashMode = .auto

        photoSettings.isDepthDataDeliveryEnabled = builtInCameraOutput.isDepthDataDeliverySupported
        guard photoSettings.isDepthDataDeliveryEnabled != false else {
            throw LMDepthLibraryError.depthDataDeliveryisNotEnabled
        }


//        let availablePreviewPhotoPixelFormat = settings.availablePreviewPhotoPixelFormatTypes[0]
//        photoSettings.previewPhotoFormat = [
//            kCVPixelBufferPixelFormatTypeKey as String : availablePreviewPhotoPixelFormat,
//            kCVPixelBufferWidthKey as String : 3024*scale,
//            kCVPixelBufferHeightKey as String : 4032*scale
//        ]

        urlsToShare = []
        builtInCameraOutput.capturePhoto(with: photoSettings, delegate: self)
    }

}

extension LMDepthLibrary: AVCapturePhotoCaptureDelegate {

     public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

         guard error == nil else {
            print("Error capturing photo: \(error!)")
            delegate.didGetMetadata(.failure(error!))
            delegate.didCaptureDepthImage(.failure(error!))
            delegate.didCaptureRGBImageInURL(.failure(error!))
            return
        }


        if let cgImage = photo.cgImageRepresentation() {

            let rgbUIImage = UIImage(cgImage: cgImage, scale: 1, orientation: .right)

            delegate.didCaptureRGBImage(.success(rgbUIImage))

            if let rgbUIImageData = rgbUIImage.jpegData(compressionQuality: 1){
                do {
                    let documentName = UUID().uuidString+".jpg"
                    let _ = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]

                   let rgbImageURL = try FileManager.default.url(for: .cachesDirectory,
                                                                       in: .userDomainMask,
                                                                       appropriateFor: nil,
                                                                       create: false)
                                                                       .appendingPathComponent(documentName)

                    try rgbUIImageData.write(to: rgbImageURL)
                    delegate.didCaptureRGBImageInURL(.success(rgbImageURL))
                    //urlsToShare.append(outputURL)
                } catch {
                    delegate.didCaptureRGBImageInURL(.failure(LMDepthLibraryError.unableToWriteRGBfile))
                    print("Unable to save image")
                }
            } else {
                delegate.didCaptureRGBImageInURL(.failure(LMDepthLibraryError.unableToWriteRGBfile))
                print("Unable to save image")
            }



        } else {
            delegate.didCaptureRGBImage(.failure(LMDepthLibraryError.notPhotoDataFound))
        }

        delegate.didGetMetadata(.success(photo.metadata))



        guard let depthData = photo.depthData?.converting(toDepthDataType: kCVPixelFormatType_DepthFloat16) else {
            delegate.didGetDephData(.failure(LMDepthLibraryError.notDepthDataMapFound))
            delegate.didCaptureDepthImage(.failure(LMDepthLibraryError.notDepthDataMapFound))
            return
        }

            let depthPixelBuffer = depthData.depthDataMap
            deepthData = depthData


            guard let intrinsicMatrix = photo.depthData?.cameraCalibrationData?.intrinsicMatrix  else {
                deepMapImage = nil
                delegate.didGetDephData(.failure(LMDepthLibraryError.notIntrisicMatrixdataFound))
                delegate.didCaptureDepthImage(.failure(LMDepthLibraryError.notIntrisicMatrixdataFound))
                return
            }
                switch depthData.depthDataType {
                case kCVPixelFormatType_DepthFloat16:
                    print("Tipo: kCVPixelFormatType_DepthFloat16")
                case kCVPixelFormatType_DepthFloat32:
                    print("Tipo: kCVPixelFormatType_DepthFloat32")
                case kCVPixelFormatType_DisparityFloat16:
                    print("Tipo: kCVPixelFormatType_DisparityFloat16")
                case kCVPixelFormatType_DisparityFloat32:
                    print("Tipo: kCVPixelFormatType_DisparityFloat32")
                default:
                    print("Tipo: uknown")

                }
                depthData.depthDataAccuracy.rawValue == 0 ? print("Acuracy: relative")
                                                          : print("Acuracy: absolute")
                print("intrinsicMatrix: \(intrinsicMatrix)")


                guard var intrinsicMatrix = depthData.cameraCalibrationData?.intrinsicMatrix,
                      let referenceDimensions = depthData.cameraCalibrationData?.intrinsicMatrixReferenceDimensions,
                      let center = depthData.cameraCalibrationData?.lensDistortionCenter,
                      let extrinsicMatrix = depthData.cameraCalibrationData?.extrinsicMatrix else {
                          delegate.didGetDephData(.failure(LMDepthLibraryError.notCameraCalibrationDataFound))
                          delegate.didCaptureDepthImage(.failure(LMDepthLibraryError.notCameraCalibrationDataFound))
                    return
                }



                let pixelBufferWidth = Int(CVPixelBufferGetWidth(depthData.depthDataMap))
                let pixelBufferHeight = Int(CVPixelBufferGetHeight(depthData.depthDataMap))

                CVPixelBufferLockBaseAddress(depthData.depthDataMap, CVPixelBufferLockFlags(rawValue: 0))

//                let depthSize = CGSize(width: pixelBufferWidth, height: pixelBufferHeight)
                let _ = CGSize(width: pixelBufferWidth, height: pixelBufferHeight)
                let ratio: Float =  Float(referenceDimensions.width) / Float(pixelBufferWidth)

                intrinsicMatrix[0][0] /= ratio
                intrinsicMatrix[1][1] /= ratio
                intrinsicMatrix[2][0] /= ratio
                intrinsicMatrix[2][1] /= ratio

                CVPixelBufferUnlockBaseAddress(depthData.depthDataMap, CVPixelBufferLockFlags(rawValue: 0))


                let centerX: CGFloat = center.x / CGFloat(ratio)
                let centerY: CGFloat = center.y / CGFloat(ratio)
                let correctedCenter = CGPoint(x: centerX, y: centerY);

                let focalX = intrinsicMatrix[0][0]
                let focalY = intrinsicMatrix[1][1]
                let principalPointX = intrinsicMatrix[2][0]
                let principalPointY = intrinsicMatrix[2][1]

                print("Principal point coords expressed in píxels: \(principalPointX),\(principalPointY)")


                // camera_pose
                let cameraPose = extrinsicMatrixToCameraPose(from: extrinsicMatrix)

                let deepthDocumentData = LMDepthImageData(dimensions: CGSize(width: CVPixelBufferGetWidth(depthPixelBuffer),
                                                                             height: CVPixelBufferGetHeight(depthPixelBuffer)),
                                                        values: depthPixelBuffer,
                                                        principalPoint: PrincipalPoint(x: Double(principalPointX),
                                                                                       y: Double(principalPointY)),
                                                        focalDistance: FocalDistance(x: Double(focalX),
                                                                                     y: Double(focalY)),
                                                        accuracy:  depthData.depthDataAccuracy.rawValue == 0 ? "Relative"
                                                                                                             : "Absolute",
                                                        correctedCenter: correctedCenter,
                                                          cameraPose: cameraPose
                )

//                if let pdfURL = createPDF(from: deepthDocumentData) {
//                    urlsToShare.append(pdfURL)
//                }

                delegate.didGetDephData(.success(deepthDocumentData))
                createDepthDataImageRepresentation()
     }






    private func createDepthDataImageRepresentation() {
        guard let deepMap = self.deepthData?.depthDataMap else {
            deepMapImage = nil
            delegate.didCaptureDepthImage(.failure(LMDepthLibraryError.notDepthDataMapFound))
            return
        }

        var orientation: UIImage.Orientation = .right
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = .right
        case .portraitUpsideDown:
            orientation = .left
        case .landscapeLeft:
            orientation = .up
        case .landscapeRight:
            orientation = .down
        default:
            break
        }

        let ciImage = CIImage(cvPixelBuffer: deepMap, options: [ .auxiliaryDepth: true ])

        let pixelBufferWidth = Int(CVPixelBufferGetWidth(deepMap))
        let pixelBufferHeight = Int(CVPixelBufferGetHeight(deepMap))

        let imageRect:CGRect = CGRect(x: 0,
                                      y: 0,
                                      width: pixelBufferWidth,
                                      height: pixelBufferHeight)
        let ciContext = CIContext()
        guard let cgImage = ciContext.createCGImage(ciImage, from: imageRect) else {
            deepMapImage = nil
            delegate.didCaptureDepthImage(.failure(LMDepthLibraryError.notDepthDataMapFound))
            return
        }

        deepMapImage = UIImage(cgImage: cgImage,
                               scale: 1.0,
                               orientation: orientation)


        CVPixelBufferLockBaseAddress(deepMap, CVPixelBufferLockFlags(rawValue: 0))
        let height = CVPixelBufferGetHeight(deepMap)
        let bpr = CVPixelBufferGetBytesPerRow(deepMap)

        if let addr = CVPixelBufferGetBaseAddress(deepMap){

            let data = Data(bytes: addr, count: (bpr*height))

                   do {
                       let documentName = UUID().uuidString
//                       let documentsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
                       let _ = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]

                      let outputURL = try FileManager.default.url(for: .cachesDirectory,
                                                                          in: .userDomainMask,
                                                                          appropriateFor: nil,
                                                                          create: false)
                                                                           .appendingPathComponent(documentName)
                                                                           .appendingPathExtension("png")

                       try data.write(to: outputURL)
                       delegate.didCaptureDepthImageInURL(.success(outputURL))
                       //urlsToShare.append(outputURL)
                   } catch {
                       delegate.didCaptureDepthImageInURL(.failure(LMDepthLibraryError.unableToWriteDepthPNGfile))
                       print("Unable to save image")
                   }
        } else {
            delegate.didCaptureDepthImageInURL(.failure(LMDepthLibraryError.unableToGetPixelBufferBaseAddress))
        }


        CVPixelBufferUnlockBaseAddress(deepMap, CVPixelBufferLockFlags(rawValue: 0))

        //delegate.didSaveDocuments(inUrls: urlsToShare)

        delegate.didCaptureDepthImage(.success(deepMapImage))
    }

    func extrinsicMatrixToCameraPose(from matrix: matrix_float4x3) -> matrix_float4x4 {
        var result = matrix_float4x4()

        // Copy the original 3x3 matrix elements
        result.columns.0 = SIMD4<Float>(matrix.columns.0, 0)
        result.columns.1 = SIMD4<Float>(matrix.columns.1, 0)
        result.columns.2 = SIMD4<Float>(matrix.columns.2, 0)

        // Set the last column to (0, 0, 0, 1)
        result.columns.3 = SIMD4<Float>(0, 0, 0, 1)

        return simd_inverse(result)
    }

}

