#import <React/RCTViewManager.h>
#import "React/RCTBridgeModule.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

 @interface RCT_EXTERN_MODULE(LogmealDepthSdkViewManager, RCTViewManager)
    RCT_EXTERN_METHOD(isDeviceAbleToCaptureDeepthData:(RCTResponseSenderBlock) callback)
    RCT_EXTERN_METHOD(configure:(RCTResponseSenderBlock) callback)

    RCT_EXTERN_METHOD(
      captureImage: (RCTPromiseResolveBlock) onSuccess
      errorPromise: (RCTPromiseRejectBlock) onError
    )
 @end
