//
//  LMDepthSDK.swift
//  LogmealDepthSdk
//
//  Created by Eduard Calero on 8/7/22.
//  Copyright © 2022 Facebook. All rights reserved.
//

import Foundation
import LogMealDepth


@objc(LMDepthSDK)
class LMDepthSDK: NSObject {

    @objc let library = LMDepthLibrary()

    @objc static func requiresMainQueueSetup() -> Bool { return true }

    @objc public func checkDeviceSupport(
      _ resolve: RCTPromiseResolveBlock,
      rejecter reject: RCTPromiseRejectBlock
    ) -> Void {
      resolve(library.isDeviceAbleToCaptureDeepthData())
    }
}
