import UIKit
import LogMealDepth


@objc(LogmealDepthSdkViewManager)
class LogmealDepthSdkViewManager: RCTViewManager {

 var sdkView: LogmealDepthSdkView?

     override func view() -> UIView {
       self.sdkView = LogmealDepthSdkView()
       return sdkView!
        // return LogmealDepthSdkView(frame: .zero)
     }

    @objc
    override static func requiresMainQueueSetup() -> Bool {
        return false
    }

    @objc
    public func isDeviceAbleToCaptureDeepthData(_ callback: RCTResponseSenderBlock) {
        sdkView?.isDeviceAbleToCaptureDeepthData(callback)
    }

    @objc
    public func configure(_ callback: RCTResponseSenderBlock){
      print("-------- CONFIGURE ---------")

        do {
                try self.sdkView?.configure()
                callback([NSNull(), "Configuration succeeded"])
        } catch let libraryError as LMDepthLibraryError {
                callback([libraryError.localizedDescription, NSNull()])
        } catch let error as NSError {
            callback([error.localizedDescription, NSNull()])

        }
    }


    @objc
    public func captureImage(_ onSuccess: @escaping RCTPromiseResolveBlock,
                errorPromise onError: @escaping RCTPromiseRejectBlock)  {
                        sdkView?.captureImage(onSuccess, errorPromise: onError)
    }


}


//TODO: Change this UIView for PreviewView
class LogmealDepthSdkView : UIView, LMDepthLibraryDelegateProtocol {




    // var completionOnSuccess: ((UIImage?,
    //                            [String : Any],
    //                            [String: Any],
    //                             UIImage?,
    //                             URL)->())? = nil
    // var completionOnError: ((Error)->())? = nil

    var completionOnSuccess: RCTPromiseResolveBlock? = nil
    var completionOnError: RCTPromiseRejectBlock? = nil

    let deppLib = LMDepthLibrary()
    var previewView: PreviewView!

    var didCaptureRGBImageResult: Result<UIImage?, Error>? = nil
    var didCaptureRGBImageInURL: Result<URL, Error>? = nil
    var didGetMetadataResult: Result<[String : Any], Error>? = nil
    var didGetDephDataResult: Result<LMDepthImageData, Error>? = nil
    var didCaptureDepthImageResult: Result<UIImage?, Error>? = nil
    var didCaptureDepthImageInURL: Result<URL, Error>? = nil

    public func isDeviceAbleToCaptureDeepthData(_ callback: RCTResponseSenderBlock) {
        callback([deppLib.isDeviceAbleToCaptureDeepthData()])
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
    }


    fileprivate func configureView(){
        DispatchQueue.main.async { [weak self] in
            let previewView = PreviewView()
            previewView.translatesAutoresizingMaskIntoConstraints = false
            if let strongSelf = self {
                strongSelf.previewView = previewView
                strongSelf.addSubview(previewView)
                NSLayoutConstraint.activate([
                    previewView.centerXAnchor.constraint(equalTo: strongSelf.centerXAnchor),
                    previewView.centerYAnchor.constraint(equalTo: strongSelf.centerYAnchor),
                    previewView.widthAnchor.constraint(equalTo: strongSelf.widthAnchor, multiplier: 1),
                    previewView.heightAnchor.constraint(equalTo: strongSelf.heightAnchor, multiplier: 1),
                ])
            }
        }
    }

    public func configure() throws {
        try deppLib.configure(delegate: self, preview: previewView)
    }

    public func captureImage(_ onSuccess: @escaping RCTPromiseResolveBlock,
                    errorPromise onError: @escaping RCTPromiseRejectBlock) {

        self.completionOnSuccess = onSuccess
        self.completionOnError = onError

        didCaptureRGBImageResult = nil
        didCaptureRGBImageInURL = nil
        didGetMetadataResult = nil
        didGetDephDataResult = nil
        didCaptureDepthImageResult = nil
        didCaptureDepthImageInURL = nil

        do {
            try deppLib.captureImage()
        } catch {
            completionOnError?("0", error.localizedDescription, error as NSError)
        }
    }


    func onError(_ error: LMDepthLibraryError) {
        completionOnError?("0", error.localizedDescription, error as NSError)
    }

    func didCaptureRGBImage(_ image: Result<UIImage?, Error>) {
//        didCaptureRGBImageResult = image
//        throwResultIfFinished()
    }

    func didCaptureRGBImageInURL(_ image: Result<URL, Error>) {
        didCaptureRGBImageInURL = image
        throwResultIfFinished()
    }

    func didGetMetadata(_ metadataDic: Result<[String : Any], Error>) {
        didGetMetadataResult = metadataDic
        throwResultIfFinished()
    }

    func didGetDephData(_ depthMetadata: Result<LMDepthImageData, Error>) {
        didGetDephDataResult = depthMetadata
        throwResultIfFinished()
    }

    func didCaptureDepthImage(_ image: Result<UIImage?, Error>) {
//        didCaptureDepthImageResult = image
//        throwResultIfFinished()
    }

    func didCaptureDepthImageInURL(_ image: Result<URL, Error>) {
            didCaptureDepthImageInURL = image
            throwResultIfFinished()
    }

    fileprivate func throwResultIfFinished() {

        guard  let _ = didCaptureRGBImageInURL,
               //let RGBImage = didCaptureRGBImageResult,
               let _ = didGetMetadataResult,
               let _ = didGetDephDataResult,
               //let depthImage = didCaptureDepthImageResult,
               let _ = didCaptureDepthImageInURL else {
                   return
               }


        guard  //case .success(let RGBImageSuccess) = didCaptureRGBImageResult,
               case .success(let rgbImageinURLSuccess) = didCaptureRGBImageInURL,
               case .success(let metadataDicSuccess) = didGetMetadataResult,
               case .success(let depthDataSuccess) = didGetDephDataResult,
            //    case .success(let depthImageSuccess) = didCaptureDepthImageResult,
               case .success(let depthimageinURLSuccess) = didCaptureDepthImageInURL else {


            if case .failure(let error) = didCaptureRGBImageInURL {
                self.completionOnError?("0", "Error getting RGB image",  error as NSError)
            } else if case .failure(let error) = didGetMetadataResult {
                self.completionOnError?("0", "Error extracting metadata", error as NSError)
            } else if case .failure(let error) = didGetDephDataResult {
                self.completionOnError?("0", "Error getting depth data", error as NSError)
            } else if case .failure(let error) = didCaptureDepthImageInURL {
                self.completionOnError?("0", "Error getting depth data image form URL", error as NSError)
            } else {
                   self.completionOnError?("0", "Error al recuperar los datos", NSError())
            }
            return
        }

        var resultDic = [String: Any]()
        var depthDic = depthDataSuccess.dic

        resultDic["depthFocalLengthX"] = depthDic["depthFocalLengthX"]
        resultDic["depthFocalLengthY"] = depthDic["depthFocalLengthY"]

        resultDic["depthPrincipalPointX"] = depthDic["depthPrincipalPointX"]
        resultDic["depthPrincipalPointY"] = depthDic["depthPrincipalPointY"]

        resultDic["depthImageWidth"] = depthDic["depthImageWidth"]
        resultDic["depthImageHeight"] = depthDic["depthImageHeight"]
        resultDic["depthImageUri"] = depthimageinURLSuccess.absoluteString as NSString

        resultDic["imageUri"] = rgbImageinURLSuccess.absoluteString as NSString

        resultDic["cameraPose"] = depthDic["cameraPose"]

        // -------------
        // EXTRA START
        // -------------

        resultDic["confidenceMapUri"] = nil

        // -------------
        // EXTRA END
        // -------------

        self.completionOnSuccess?(resultDic)
    }

}
