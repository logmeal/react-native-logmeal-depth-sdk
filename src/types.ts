import type { ViewStyle } from 'react-native';

export interface IDepthImageData {
  focalLength: {
    x: number;
    y: number;
  };
  principalPoint: {
    x: number;
    y: number;
  };
  size: {
    width: number;
    height: number;
  };
  uri: string;
}

export interface ITakePhotoResponse {
  cameraPose: number[];
  depthData: IDepthImageData;
  rgbImageUri: string;
  extra: {
    rawDepthData: IDepthImageData | null;
    confidenceMapUri: string;
  };
}

export interface ILogmealSdkDepthCameraProps {
  style: ViewStyle;
}
