// @ts-nocheck
import React, { createRef, PureComponent } from 'react';
import {
  NativeMethods,
  NativeModules,
  Platform,
  requireNativeComponent,
  StyleSheet,
  UIManager,
  View,
} from 'react-native';
import type { ILogmealSdkDepthCameraProps, ITakePhotoResponse } from './types';

const { LMDepthSDK, LogmealDepthSdkViewManager } = NativeModules;

const LINKING_ERROR =
  `The package 'react-native-logmeal-sdk' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const ComponentName = 'LogmealDepthSdkView';

const NativeDepthCamera =
  UIManager.getViewManagerConfig(ComponentName) != null
    ? requireNativeComponent(ComponentName)
    : () => {
        throw new Error(LINKING_ERROR);
      };

const presetDimensions = { width: 640, height: 480 };
const presetAspectRatios = {
  normal: presetDimensions.width / presetDimensions.height,
  inverse: presetDimensions.height / presetDimensions.width,
};
class DepthCamera extends PureComponent<ILogmealSdkDepthCameraProps> {
  private readonly ref: React.RefObject<DepthCameraRefType>;

  constructor(props: ILogmealSdkDepthCameraProps) {
    super(props);
    this.ref = createRef();
  }

  public async takePhoto(): Promise<ITakePhotoResponse> {
    const result = await LogmealDepthSdkViewManager.captureImage();

    if (Platform.OS === 'ios') {
      return {
        cameraPose: result.cameraPose,
        depthData: {
          focalLength: {
            x: result.depthFocalLengthX,
            y: result.depthFocalLengthY,
          },
          principalPoint: {
            x: result.depthPrincipalPointX,
            y: result.depthPrincipalPointY,
          },
          size: {
            width: result.depthImageWidth,
            height: result.depthImageHeight,
          },
          uri: result.depthImageUri,
        },
        rgbImageUri: result.imageUri,
        extra: {
          rawDepthData: null,
          confidenceMapUri: '',
        },
      };
    } else {
      throw new Error('Platform not supporteds');
    }
  }

  componentDidMount(): void {
    setTimeout(
      () =>
        LogmealDepthSdkViewManager.configure((error: any, _result: any) => {
          if (error) {
            throw new Error(error ?? 'Error configuring depth camera');
          }
        }),
      100
    );
  }

  public render(): React.ReactNode {
    const { style, ...props } = this.props;

    const { width: styleWidth, height: styleHeight, ...rest } = style;

    const width = styleWidth ?? presetDimensions.width;
    const height = styleHeight ?? presetDimensions.height;
    let newWidth;
    let newHeight;
    let marginTop = 0;
    let marginLeft = 0;

    if (height > width) {
      newHeight = height;
      newWidth = height * presetAspectRatios.inverse;
      marginLeft = (newWidth / 2 - width / 2) * -1;
    } else {
      newWidth = width;
      newHeight = width * presetAspectRatios.normal;
      marginTop = (newHeight / 2 - height / 2) * -1;
    }

    const stylesToCenter = {
      width: newWidth,
      height: newHeight,
      marginTop,
      marginLeft,
    };

    return (
      <View style={[rest, styles.overflowHidden]}>
        <NativeDepthCamera
          {...props}
          style={[style, stylesToCenter]}
          ref={this.ref}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overflowHidden: {
    overflow: 'hidden',
  },
});

const checkDeviceSupport = LMDepthSDK.checkDeviceSupport;

export { checkDeviceSupport, DepthCamera };

export type DepthCameraRefType = PureComponent<ILogmealSdkDepthCameraProps> &
  Readonly<NativeMethods> &
  Readonly<{ takePhoto: () => Promise<ITakePhotoResponse> }>;
