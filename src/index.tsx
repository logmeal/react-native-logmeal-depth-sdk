// @ts-nocheck
import {
  checkDeviceSupport,
  DepthCamera,
  DepthCameraRefType,
} from './DepthCamera';

import type { ITakePhotoResponse } from './types';

export {
  checkDeviceSupport,
  DepthCamera,
  DepthCameraRefType,
  ITakePhotoResponse,
};
