// @ts-nocheck
import React, { createRef, PureComponent } from 'react';
import {
  findNodeHandle,
  NativeMethods,
  NativeModules,
  Platform,
  requireNativeComponent,
  UIManager,
} from 'react-native';
import type { ILogmealSdkDepthCameraProps, ITakePhotoResponse } from './types';

const { LMDepthSDK, LogmealDepthSdkRNModule, LogmealDepthSdkViewManager } =
  NativeModules;

const LINKING_ERROR =
  `The package 'react-native-logmeal-sdk' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const ComponentName =
  Platform.OS === 'ios' ? 'LogmealDepthSdkView' : 'DepthCamera';

const NativeDepthCamera =
  UIManager.getViewManagerConfig(ComponentName) != null
    ? requireNativeComponent(ComponentName)
    : () => {
        throw new Error(LINKING_ERROR);
      };

class DepthCamera extends PureComponent<ILogmealSdkDepthCameraProps> {
  private readonly ref: React.RefObject<DepthCameraRefType>;

  constructor(props: ILogmealSdkDepthCameraProps) {
    super(props);
    this.ref = createRef();
  }

  componentWillUnmount(): void {
    if (this._handle != null) {
      LogmealDepthSdkRNModule.pauseCamera(this._handle);
    }
  }

  private get _handle(): number | null {
    const nodeHandle = findNodeHandle(this.ref.current);

    if (nodeHandle == null || nodeHandle === -1) {
      throw new Error(
        "Could not get the Camera's native view tag! Does the Camera View exist in the native view-tree?"
      );
    }

    return nodeHandle;
  }

  public async takePhoto(): Promise<ITakePhotoResponse> {
    if (this._handle == null) {
      throw new Error('Camera not initialized');
    }

    if (Platform.OS === 'android') {
      const result = await LogmealDepthSdkRNModule.takePhoto(this._handle);
      return {
        cameraPose: result.cameraPose,
        depthData: {
          focalLength: {
            x: result.depthFocalLengthX,
            y: result.depthFocalLengthY,
          },
          principalPoint: {
            x: result.depthPrincipalPointX,
            y: result.depthPrincipalPointY,
          },
          size: {
            width: result.depthImageWidth,
            height: result.depthImageHeight,
          },
          uri: result.depthImageUri,
        },
        rgbImageUri: result.imageUri,
        extra: {
          rawDepthData: {
            focalLength: {
              x: result.rawDepthFocalLengthX,
              y: result.rawDepthFocalLengthY,
            },
            principalPoint: {
              x: result.rawDepthPrincipalPointX,
              y: result.rawDepthPrincipalPointY,
            },
            size: {
              width: result.rawDepthImageWidth,
              height: result.rawDepthImageHeight,
            },
            uri: result.rawDepthImageUri,
          },
          confidenceMapUri: result.confidenceMapUri,
        },
      };
    } else {
      throw new Error('Platform not supporteds');
    }
  }

  public render(): React.ReactNode {
    return <NativeDepthCamera {...this.props} ref={this.ref} />;
  }
}

const checkDeviceSupport =
  Platform.OS === 'android'
    ? LogmealDepthSdkRNModule.checkDeviceSupport
    : Platform.OS === 'ios'
    ? LMDepthSDK.checkDeviceSupport
    : () => {
        throw new Error('Platform not supported');
      };

export { checkDeviceSupport, DepthCamera };

export type DepthCameraRefType = PureComponent<ILogmealSdkDepthCameraProps> &
  Readonly<NativeMethods> &
  Readonly<{ takePhoto: () => Promise<ITakePhotoResponse> }>;
