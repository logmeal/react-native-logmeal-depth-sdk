# react-native-logmeal-depth-sdk

react-native integration for LogMeal's Depth SDK

## Installation

```sh
npm install react-native-logmeal-depth-sdk
```

## Usage

```js
import { LogmealDepthSdkView } from "react-native-logmeal-depth-sdk";

// ...

<LogmealDepthSdkView color="tomato" />
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.
