package es.logmeal.sdk;

public class CameraInfo {
    private final float[] focalLength;
    private final float[] principalPoint;
    private final float[] cameraPose;

    public CameraInfo(float[] focalLength, float[] principalPoint, float[] cameraPose) {
        this.focalLength = focalLength;
        this.principalPoint = principalPoint;
        this.cameraPose = cameraPose;
    }

    public float[] getFocalLength() {
        return focalLength;
    }

    public float[] getPrincipalPoint() {
        return principalPoint;
    }

    public float[] getCameraPose() {
        return cameraPose;
    }

    public String focalLengthToString() {
        return String.format("%s,%s", focalLength[0], focalLength[1]);
    }

    public String principalPointToString() {
        return String.format("%s,%s", principalPoint[0], principalPoint[1]);
    }

    public String cameraPoseToString() {
        StringBuilder out = new StringBuilder("[");
        for (float v : cameraPose) {
            out.append(v).append(",");
        }
        out = new StringBuilder(out.substring(0, out.length() - 1) + "]");
        return out.toString();
    }
}
