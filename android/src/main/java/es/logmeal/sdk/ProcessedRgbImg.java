package es.logmeal.sdk;

import android.content.res.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.File;
import java.lang.reflect.Type;

public class ProcessedRgbImg {
    private final File imageRGBFile;
    private final int originalWidth;
    private final int width;
    private final int originalHeight;
    private final int height;
    private final int scalingFactor;
    private final int cropDifference;
    private final File sourceRGBFile;
    private final int sourceWidth;
    private final int sourceHeight;


    public ProcessedRgbImg(File imageRGBFile, int width, int height, int scalingFactor, int cropDifference, int orientation, File sourceRGBFile, int sourceWidth, int sourceHeight) {
        this.imageRGBFile = imageRGBFile;
        this.originalWidth = width;
        this.originalHeight = height;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            this.width = height;
            this.height = width;
        } else {
            this.width = width;
            this.height = height;
        }
        this.scalingFactor = scalingFactor;
        this.cropDifference = cropDifference;
        this.sourceRGBFile = sourceRGBFile;
        this.sourceWidth = sourceWidth;
        this.sourceHeight = sourceHeight;
    }

    public File getImageRGBFile() {
        return imageRGBFile;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getScalingFactor() {
        return scalingFactor;
    }

    public int getCropDifference() {
        return cropDifference;
    }

    public int getOriginalWidth() {
        return originalWidth;
    }

    public int getOriginalHeight() {
        return originalHeight;
    }

    public File getSourceRGBFile() {
        return sourceRGBFile;
    }

    public int getSourceWidth() {
        return sourceWidth;
    }

    public int getSourceHeight() {
        return sourceHeight;
    }

    public String toJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ProcessedRgbImg.class, new ProcessedRgbImg.ProcessedRgbImgSerializer())
                .create();
        return gson.toJson(this);
    }

    public String toPrettyJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ProcessedRgbImg.class, new ProcessedRgbImg.ProcessedRgbImgSerializer())
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }

    protected static class ProcessedRgbImgSerializer implements JsonSerializer<ProcessedRgbImg> {
        @Override
        public JsonObject serialize(ProcessedRgbImg processedRgbImg, Type type, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("imageRGBFile", new JsonPrimitive(processedRgbImg.getImageRGBFile().getPath()));
            jsonObject.add("originalWidth", new JsonPrimitive(processedRgbImg.getOriginalWidth()));
            jsonObject.add("width", new JsonPrimitive(processedRgbImg.getWidth()));
            jsonObject.add("originalHeight", new JsonPrimitive(processedRgbImg.getOriginalHeight()));
            jsonObject.add("height", new JsonPrimitive(processedRgbImg.getHeight()));
            jsonObject.add("scalingFactor", new JsonPrimitive(processedRgbImg.getScalingFactor()));
            jsonObject.add("cropDifference", new JsonPrimitive(processedRgbImg.getCropDifference()));
            jsonObject.add("sourceRGBFile", new JsonPrimitive(processedRgbImg.getSourceRGBFile().getPath()));
            jsonObject.add("sourceWidth", new JsonPrimitive(processedRgbImg.getSourceWidth()));
            jsonObject.add("sourceHeight", new JsonPrimitive(processedRgbImg.getSourceHeight()));
            return jsonObject;
        }
    }
}
