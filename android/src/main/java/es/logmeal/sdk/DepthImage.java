package es.logmeal.sdk;

import android.content.res.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.File;
import java.lang.reflect.Type;

public class DepthImage {
    private final File depthFile;
    private final int originalWidth;
    private final int width;
    private final int originalHeight;
    private final int height;

    public DepthImage(File depthFile, int width, int height, int orientation) {
        this.depthFile = depthFile;
        this.originalWidth = width;
        this.originalHeight = height;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            this.width = height;
            this.height = width;
        } else {
            this.width = width;
            this.height = height;
        }
    }

    public File getDepthFile() {
        return depthFile;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getOriginalWidth() {
        return originalWidth;
    }

    public int getOriginalHeight() {
        return originalHeight;
    }

    public String toJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(DepthImage.class, new DepthImageSerializer())
                .create();
        return gson.toJson(this);
    }

    public String toPrettyJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(DepthImage.class, new DepthImageSerializer())
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }

    protected static class DepthImageSerializer implements JsonSerializer<DepthImage> {
        @Override
        public JsonObject serialize(DepthImage depthImage, Type type, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("depthFile", new JsonPrimitive(depthImage.getDepthFile().getPath()));
            jsonObject.add("originalWidth", new JsonPrimitive(depthImage.getOriginalWidth()));
            jsonObject.add("width", new JsonPrimitive(depthImage.getWidth()));
            jsonObject.add("originalHeight", new JsonPrimitive(depthImage.getOriginalHeight()));
            jsonObject.add("height", new JsonPrimitive(depthImage.getHeight()));
            return jsonObject;
        }
    }
}
