package es.logmeal.sdk.widgets;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.NotYetAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import es.logmeal.sdk.FrameDepthInfo;
import es.logmeal.sdk.FrameProcessor;
import es.logmeal.sdk.exceptions.IncompatibleRgbImageType;
import es.logmeal.sdk.helpers.DisplayRotationHelper;
import es.logmeal.sdk.helpers.TrackingStateHelper;
import es.logmeal.sdk.rendering.BackgroundRenderer;

public class DepthCamera extends GLSurfaceView implements GLSurfaceView.Renderer, LifecycleEventObserver {
    private static final String TAG = "LOGMEAL_SDK_DEPTH_CAMERA";

    private enum AppState {IDLE, RECORDING}

    private final Object frameInUseLock = new Object();
    private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
    private final TrackingStateHelper trackingStateHelper = new TrackingStateHelper((Activity) getContext());
    private final AtomicReference<AppState> currentState = new AtomicReference<>(AppState.IDLE);
    private Session session;
    private String customDepthInfoStorePath;
    private FrameDepthInfo frameDepthInfo;
    private DisplayRotationHelper displayRotationHelper;

    public DepthCamera(Context context) {
        super(context);
    }

    public DepthCamera(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onCreate() {
        displayRotationHelper = new DisplayRotationHelper(getContext());

        this.setPreserveEGLContextOnPause(true);
        this.setEGLContextClientVersion(2);
        this.setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        this.setRenderer(this);
        this.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        this.setWillNotDraw(false);

        // Workaround for session to get the correct config
        // setting the same config in this method won't work
        // onResume();
        // onPause();
        // onResume();
    }

    public CompletableFuture<FrameDepthInfo> captureNextFrame() {
        currentState.set(AppState.RECORDING);

        CompletableFuture<FrameDepthInfo> future = new CompletableFuture<>();
        future.thenAccept(frameDepthInfo -> {
            // Reset the frameDepthInfo after processing
            synchronized (frameInUseLock) {
                DepthCamera.this.frameDepthInfo = null;
            }
        });

        // Set the future result once frameDepthInfo is available
        synchronized (frameInUseLock) {
            if (frameDepthInfo != null) {
                future.complete(frameDepthInfo);
            } else {
                // If frameDepthInfo is not available, wait until it becomes available or timeout occurs
                new Thread(() -> {
                    long startTime = System.currentTimeMillis();
                    long timeoutMillis = Duration.ofMinutes(1).toMillis();
                    long elapsedTime;
                    try {
                        while (frameDepthInfo == null) {
                            elapsedTime = System.currentTimeMillis() - startTime;
                            if (elapsedTime >= timeoutMillis) {
                                future.completeExceptionally(new TimeoutException("Timeout exceeded"));
                                break;
                            }
                            TimeUnit.MILLISECONDS.sleep(100);
                        }
                        if (!future.isDone()) {
                            future.complete(frameDepthInfo);
                        }
                    } catch (InterruptedException e) {
                        future.completeExceptionally(e);
                    }
                }).start();
            }
        }

        return future;
    }

    @Override
    public void onResume() {

        try {
            if (session == null) {
                session = new Session(getContext());
            }
        } catch (UnavailableDeviceNotCompatibleException | UnavailableSdkTooOldException |
                 UnavailableApkTooOldException | UnavailableArcoreNotInstalledException e) {
            Log.e(TAG, "Exception creating session", e);
        }

        try {
            synchronized (frameInUseLock) {
                // Enable raw depth estimation and auto focus mode while ARCore is running.
                Config config = session.getConfig();
                config.setFocusMode(Config.FocusMode.AUTO);
                config.setDepthMode(Config.DepthMode.AUTOMATIC);
                session.configure(config);
                session.resume();
            }
        } catch (CameraNotAvailableException e) {
            session = null;
            return;
        }

        super.onResume();
        displayRotationHelper.onResume();
    }

    protected void onDestroy() {
        if (session != null) {
            session.close();
            session = null;
        }
    }

    @Override
    public void onPause() {
        if (session != null) {
            // Note that the order matters - see note in onResume().
            // GLSurfaceView is paused before pausing the ARCore session, to prevent onDrawFrame() from
            // calling session.update() on a paused session.
            displayRotationHelper.onPause();
            super.onPause();
            if (currentState.get() == AppState.RECORDING) {
                currentState.set(AppState.IDLE);
            }
            session.pause();
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
        try {
            backgroundRenderer.createOnGlThread(getContext());
        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        // Do not render anything or call session methods until session is created.
        if (session == null) {
            return;
        }

        displayRotationHelper.updateSessionIfNeeded(session);

        try {
            session.setCameraTextureName(backgroundRenderer.getTextureId());

            Frame frame = session.update();
            Camera camera = frame.getCamera();
            backgroundRenderer.draw(frame);

            trackingStateHelper.updateKeepScreenOnFlag(camera.getTrackingState());
            if (camera.getTrackingState() != TrackingState.TRACKING) {
                return;
            }

            if (currentState.get() == AppState.RECORDING) {
                currentState.set(AppState.IDLE);
                Log.d(TAG, "@@@@ Recording frame " + customDepthInfoStorePath);
                FrameProcessor fp = customDepthInfoStorePath != null
                        ? new FrameProcessor(frame, session, getContext(), customDepthInfoStorePath)
                        : new FrameProcessor(frame, session, getContext());
                synchronized (frameInUseLock) {
                    DepthCamera.this.frameDepthInfo = fp.getDepthInfo();
                }
            }
        } catch (NotYetAvailableException | CameraNotAvailableException | IncompatibleRgbImageType |
                 IOException e) {
            Log.e(TAG, "Exception on the OpenGL thread", e);
        }
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        switch (event) {
            case ON_CREATE:
                this.onCreate();
                break;
            case ON_RESUME:
                this.onResume();
                break;
            case ON_DESTROY:
                this.onDestroy();
                break;
            case ON_PAUSE:
                this.onPause();
                break;
        }
    }

    public String getCustomDepthInfoStorePath() {
        return customDepthInfoStorePath;
    }

    public void setCustomDepthInfoStorePath(String customDepthInfoStorePath) {
        this.customDepthInfoStorePath = customDepthInfoStorePath;
    }
}
