package es.logmeal.sdk;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;

import androidx.exifinterface.media.ExifInterface;
import android.media.Image;

import com.google.ar.core.Anchor;
import com.google.ar.core.CameraIntrinsics;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.NotYetAvailableException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import es.logmeal.sdk.exceptions.IncompatibleRgbImageType;
import es.logmeal.sdk.helpers.ImageConvertionUtils;

public class FrameProcessor {
    private static final String TAG = "LOGMEAL_SDK_DEPTH_FRAME_PROCESSOR";
    private final Frame frame;
    private final Session session;
    private final long identifier;
    private final int orientation;
    private String outputPath;

    public FrameProcessor(Frame frame, Session session, Context context) {
        this.frame = frame;
        this.session = session;
        this.identifier = System.currentTimeMillis();
        this.orientation = context.getResources().getConfiguration().orientation;
        this.outputPath = null;
    }

    public FrameProcessor(Frame frame, Session session, Context context, String outputPath) {
        this.frame = frame;
        this.session = session;
        this.identifier = System.currentTimeMillis();
        this.orientation = context.getResources().getConfiguration().orientation;
        this.outputPath = outputPath;
    }

    private DepthImage saveDepthData(Image image, String suffix) throws IOException {
        int width = image.getWidth();
        int height = image.getHeight();

        Image.Plane plane = image.getPlanes()[0];
        ByteBuffer buffer = plane.getBuffer().order(ByteOrder.nativeOrder());
        buffer.position(0);
        File depthFile = prepareOutputFile(suffix, ".png");
        FileChannel fc = new FileOutputStream(depthFile).getChannel();
        fc.write(buffer);
        fc.close();

        return new DepthImage(depthFile, width, height, orientation);
    }

    private ProcessedRgbImg getImageData(int cropWidth, int cropHeight) throws NotYetAvailableException, IncompatibleRgbImageType, IOException {
        Image originalImage = frame.acquireCameraImage();
        int originalImageWidth = originalImage.getWidth();
        int originalImageHeight = originalImage.getHeight();

        int format = originalImage.getFormat();
        if (format != ImageFormat.YUV_420_888) {
            throw new IncompatibleRgbImageType();
        }

        byte[] jpegImage = ImageConvertionUtils.toJpegImageBytes(originalImage, 100);
        File originalImageFile = prepareOutputFile("_original_RGB", ".jpeg");
        FileChannel fc = new FileOutputStream(originalImageFile).getChannel();
        fc.write(ByteBuffer.wrap(jpegImage));
        fc.close();
        originalImage.close();

        // cropping dimensions
        int scalingFactor = originalImageWidth / cropWidth;
        int desiredWidth = cropWidth * scalingFactor;
        int desiredHeight = cropHeight * scalingFactor;
        int xOffset = (originalImageWidth - desiredWidth) / 2;
        int yOffset = (originalImageHeight - desiredHeight) / 2;

        // no cropping
        if (xOffset == 0 && yOffset == 0) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                ExifInterface exifInterface = new ExifInterface(originalImageFile.getPath());
                exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(ExifInterface.ORIENTATION_ROTATE_90));
                exifInterface.saveAttributes();
            }
            return new ProcessedRgbImg(originalImageFile, originalImageWidth, originalImageHeight, 0, 0, orientation, null, originalImageWidth, originalImageHeight);
        }

        Bitmap originalImageBitmap = BitmapFactory.decodeFile(originalImageFile.getAbsolutePath());
        Bitmap croppedBitmap = Bitmap.createBitmap(originalImageBitmap, xOffset, yOffset, desiredWidth, desiredHeight);
        File croppedImageFile = prepareOutputFile("_cropped_RGB", ".jpeg");
        FileOutputStream fos = new FileOutputStream(croppedImageFile);
        croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        fos.close();

        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            ExifInterface exifInterface = new ExifInterface(croppedImageFile.getPath());
            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(ExifInterface.ORIENTATION_ROTATE_90));
            exifInterface.saveAttributes();
        }
        return new ProcessedRgbImg(croppedImageFile, desiredWidth, desiredHeight, scalingFactor, yOffset, orientation, originalImageFile, originalImageWidth, originalImageHeight);
    }

    private float[] getDepthFocalLength(Image image)  {
        CameraIntrinsics intrinsics = frame.getCamera().getTextureIntrinsics();
        int[] intrinsicsDimensions = intrinsics.getImageDimensions();
        int depthWidth = image.getWidth();
        int depthHeight = image.getHeight();

        float[] focalLength = new float[2];
        focalLength[0] = intrinsics.getFocalLength()[0] * depthWidth / intrinsicsDimensions[0];
        focalLength[1] = intrinsics.getFocalLength()[1] * depthHeight / intrinsicsDimensions[1];
        return focalLength;
    }

    private float[] getDepthPrincipalPoint(Image image)  {
        CameraIntrinsics intrinsics = frame.getCamera().getTextureIntrinsics();
        int[] intrinsicsDimensions = intrinsics.getImageDimensions();
        int depthWidth = image.getWidth();
        int depthHeight = image.getHeight();

        float[] principalPoint = new float[2];
        principalPoint[0] = intrinsics.getPrincipalPoint()[0] * depthWidth / intrinsicsDimensions[0];
        principalPoint[1] = intrinsics.getPrincipalPoint()[1] * depthHeight / intrinsicsDimensions[1];
        return principalPoint;
    }

    private CameraInfo getCameraInfo(ProcessedRgbImg imageData) {
        Anchor cameraPoseAnchor = session.createAnchor(frame.getCamera().getPose());
        float[] poseMatrix = new float[16];
        cameraPoseAnchor.getPose().toMatrix(poseMatrix, 0);

        float[] focalLength = frame.getCamera().getImageIntrinsics().getFocalLength();
        float[] principalPoint = frame.getCamera().getImageIntrinsics().getPrincipalPoint();

        if (imageData.getCropDifference() > 0) {
            principalPoint[0] -= imageData.getCropDifference();
        }

        return new CameraInfo(focalLength, principalPoint, poseMatrix);
    }

    public FrameDepthInfo getDepthInfo() throws NotYetAvailableException, IOException, IncompatibleRgbImageType {
        Image depth16Img = frame.acquireDepthImage16Bits();
        DepthImage depthImage = saveDepthData(depth16Img, "_depth16");
        DepthInfo depthInfo = new DepthInfo(getDepthFocalLength(depth16Img), getDepthPrincipalPoint(depth16Img));

        Image rawDepthImg = frame.acquireRawDepthImage16Bits();
        DepthImage depthImageRaw = saveDepthData(rawDepthImg, "_raw_depth16");
        DepthInfo rawDepthInfo = new DepthInfo(getDepthFocalLength(rawDepthImg), getDepthPrincipalPoint(rawDepthImg));

        Image confidenceImg = frame.acquireRawDepthConfidenceImage();
        DepthImage confidenceMap = saveDepthData(confidenceImg, "_confidence_map");

        ProcessedRgbImg imageData = getImageData(depthImage.getOriginalWidth(), depthImage.getOriginalHeight());
        CameraInfo ci = getCameraInfo(imageData);

        depth16Img.close();
        rawDepthImg.close();
        confidenceImg.close();
        return new FrameDepthInfo(depthImage, depthImageRaw, confidenceMap, depthInfo, rawDepthInfo, imageData, ci);
    }

    public FrameDepthInfo saveDepthInfo() throws NotYetAvailableException, IOException, IncompatibleRgbImageType {
        FrameDepthInfo depthInfo = getDepthInfo();
        File infoJson = prepareOutputFile("_info", ".json");
        FileOutputStream fos = new FileOutputStream(infoJson);
        fos.write(depthInfo.toPrettyJson().getBytes());
        fos.close();
        return depthInfo;
    }

    public String getOutputPath() {
        return outputPath;
    }
    public void setOutputPath(String outputPath) { this.outputPath = outputPath; }

    private File prepareOutputFile(String prefix, String suffix) throws IOException {
        return outputPath == null
                ? File.createTempFile(identifier + prefix, suffix)
                : new File(outputPath, identifier + prefix + suffix);
    }
}
