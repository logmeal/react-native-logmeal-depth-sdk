package es.logmeal.sdk;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FrameDepthInfo {
    private final DepthImage depthImage;
    private final DepthImage rawDepthImage;
    private final DepthImage confidenceMap;
    private final DepthInfo depthInfo;
    private final DepthInfo rawDepthInfo;
    private final ProcessedRgbImg processedRgbImg;
    private final CameraInfo cameraInfo;

    public FrameDepthInfo(
            DepthImage depthImage,
            DepthImage rawDepthImage,
            DepthImage confidenceMap,
            DepthInfo depthInfo,
            DepthInfo rawDepthInfo,
            ProcessedRgbImg imageData,
            CameraInfo cameraInfo) {
        this.depthImage = depthImage;
        this.rawDepthImage = rawDepthImage;
        this.confidenceMap = confidenceMap;
        this.depthInfo = depthInfo;
        this.rawDepthInfo = rawDepthInfo;
        this.processedRgbImg = imageData;
        this.cameraInfo = cameraInfo;
    }

    public DepthImage getDepthImage() {
        return depthImage;
    }

    public DepthImage getRawDepthImage() {
        return rawDepthImage;
    }

    public DepthImage getConfidenceMap() {
        return confidenceMap;
    }

    public DepthInfo getDepthInfo() {
        return depthInfo;
    }

    public DepthInfo getRawDepthInfo() {
        return rawDepthInfo;
    }

    public ProcessedRgbImg getProcessedRgbImg() {
        return processedRgbImg;
    }

    public CameraInfo getCameraInfo() {
        return cameraInfo;
    }

    public String toJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(DepthImage.class, new DepthImage.DepthImageSerializer())
                .registerTypeAdapter(ProcessedRgbImg.class, new ProcessedRgbImg.ProcessedRgbImgSerializer())
                .create();
        return gson.toJson(this);
    }

    public String toPrettyJson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(DepthImage.class, new DepthImage.DepthImageSerializer())
                .registerTypeAdapter(ProcessedRgbImg.class, new ProcessedRgbImg.ProcessedRgbImgSerializer())
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }
}
