package com.reactnativelogmealdepthsdk;

import android.app.Activity;
import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.react.uimanager.ThemedReactContext;
import com.google.ar.core.exceptions.NotYetAvailableException;

import java.util.concurrent.CompletableFuture;

import es.logmeal.sdk.FrameDepthInfo;

public class LogmealDepthSdkDepthCameraMainView extends LinearLayout {

  LogmealDepthSdkDepthCameraView depthCameraView;
  Activity mActivity;

  public LogmealDepthSdkDepthCameraMainView(ThemedReactContext context, Activity activity)
  {
    super(context);
    mActivity = activity;
    this.setOrientation(LinearLayout.VERTICAL);
    this.setBackgroundColor(Color.TRANSPARENT);
    this.depthCameraView = new LogmealDepthSdkDepthCameraView(context);
    this.addView(depthCameraView);
    setLayoutParams(new android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT));
  }

  public CompletableFuture<FrameDepthInfo> takePhoto() {
      return depthCameraView.captureNextFrame();
  }

  public void pauseCamera() {
    depthCameraView.onHostPause();
  }
}
