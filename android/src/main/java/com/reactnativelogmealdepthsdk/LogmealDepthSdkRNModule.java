package com.reactnativelogmealdepthsdk;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;


import com.google.ar.core.exceptions.NotYetAvailableException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import es.logmeal.sdk.FrameDepthInfo;
import es.logmeal.sdk.helpers.ArCoreSupport;
import es.logmeal.sdk.helpers.ArCoreUtils;

public class LogmealDepthSdkRNModule extends ReactContextBaseJavaModule {
  public static final String REACT_CLASS = "LogmealDepthSdkRNModule";
  private final ReactApplicationContext reactContext;

  public LogmealDepthSdkRNModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {

    }
  };

  @ReactMethod
  public void pauseCamera(int viewTag) {
    LogmealDepthSdkDepthCameraMainView mainView = findDepthCameraMainView(viewTag);
    if (mainView != null) {
      mainView.pauseCamera();
    }
  }

  @ReactMethod
  public void checkDeviceSupport(Promise promise) {

    ArCoreSupport arCoreSupport = ArCoreUtils.isDeviceSupported((AppCompatActivity) getActivity());

    if (arCoreSupport.isArCoreSupported()) {
      if (arCoreSupport.isDepthSupported()) {
        promise.resolve(true);
      } else {
        promise.resolve(false);
      }
    } else {
      promise.resolve(false);
    }
  }

  @ReactMethod
  public void takePhoto(int viewTag, Promise promise) {
    LogmealDepthSdkDepthCameraMainView mainView = findDepthCameraMainView(viewTag);

    if (mainView == null) {
      promise.reject(new RuntimeException("Could not find view with tag: " + viewTag));
    }

    // Capture the next frame and retrieve the CompletableFuture
    CompletableFuture<FrameDepthInfo> future = mainView.takePhoto();

    // Attach a callback to the CompletableFuture
    future.thenAccept(frameDepthInfo -> {
      WritableMap responseMap = Arguments.createMap();
      responseMap.putDouble("depthFocalLengthX", frameDepthInfo.getDepthInfo().getFocalLength()[0]);
      responseMap.putDouble("depthFocalLengthY", frameDepthInfo.getDepthInfo().getFocalLength()[1]);

      responseMap.putDouble("depthPrincipalPointX", frameDepthInfo.getDepthInfo().getPrincipalPoint()[0]);
      responseMap.putDouble("depthPrincipalPointY", frameDepthInfo.getDepthInfo().getPrincipalPoint()[1]);

      responseMap.putDouble("depthImageWidth", frameDepthInfo.getDepthImage().getWidth());
      responseMap.putDouble("depthImageHeight", frameDepthInfo.getDepthImage().getHeight());
      responseMap.putString("depthImageUri", String.format("file://%s", frameDepthInfo.getDepthImage().getDepthFile().getAbsolutePath()));

      responseMap.putString("imageUri", String.format("file://%s", frameDepthInfo.getProcessedRgbImg().getImageRGBFile().getAbsolutePath()));

      List<Float> camPoseList = new ArrayList<>();
      for (float value : frameDepthInfo.getCameraInfo().getCameraPose()) {
        camPoseList.add(value);
      }
      WritableArray cameraPoseWA = Arguments.fromList(camPoseList);
      responseMap.putArray("cameraPose", cameraPoseWA);

      // -------------
      // EXTRA START
      // -------------

      // info RAW
      responseMap.putDouble("rawDepthFocalLengthX", frameDepthInfo.getRawDepthInfo().getFocalLength()[0]);
      responseMap.putDouble("rawDepthFocalLengthY", frameDepthInfo.getRawDepthInfo().getFocalLength()[1]);

      responseMap.putDouble("rawDepthPrincipalPointX", frameDepthInfo.getRawDepthInfo().getPrincipalPoint()[0]);
      responseMap.putDouble("rawDepthPrincipalPointY", frameDepthInfo.getRawDepthInfo().getPrincipalPoint()[1]);

      responseMap.putDouble("rawDepthImageWidth", frameDepthInfo.getRawDepthImage().getWidth());
      responseMap.putDouble("rawDepthImageHeight", frameDepthInfo.getRawDepthImage().getHeight());
      responseMap.putString("rawDepthImageUri", String.format("file://%s", frameDepthInfo.getRawDepthImage().getDepthFile().getAbsolutePath()));

      // info confidence
      responseMap.putString("confidenceMapUri", String.format("file://%s", frameDepthInfo.getConfidenceMap().getDepthFile().getAbsolutePath()));

      // -------------
      // EXTRA END
      // -------------

      promise.resolve(responseMap);

    });

    future.exceptionally(throwable -> {
      promise.reject(new RuntimeException(throwable));
      return null;
    });

  }

  private LogmealDepthSdkDepthCameraMainView findDepthCameraMainView(int id) {
    return getActivity().findViewById(id);
  }

  @Override
  @NonNull
  public String getName() {
    return REACT_CLASS;
  }

  public Activity getActivity() {
    return this.getCurrentActivity();
  }
}
