package com.reactnativelogmealdepthsdk;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;

public class LogmealDepthSdkViewManager extends ViewGroupManager<LogmealDepthSdkDepthCameraMainView> {
    public static final String REACT_CLASS = "DepthCamera";

  private LogmealDepthSdkRNModule mContextModule;

  public LogmealDepthSdkViewManager(ReactApplicationContext reactContext) {
    mContextModule = new LogmealDepthSdkRNModule(reactContext);
  }

  @Override
  @NonNull
  public String getName() {
    return REACT_CLASS;
  }

  @Override
  protected LogmealDepthSdkDepthCameraMainView createViewInstance(ThemedReactContext reactContext) {
    return new LogmealDepthSdkDepthCameraMainView(reactContext, mContextModule.getActivity());
  }
}
